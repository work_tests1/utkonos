import os
from starlette.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_root_200(test_app):
    response = test_app.get("/")
    assert response.status_code == 200
    assert response.json() == {'message': 'I am Root'}


def test_root_400(test_app):
    response = test_app.get("/not-exist")
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_post_empty_comment(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "9661111111",
                "email": "any@mail.ru",
                "comment": ""
                }
            )
    assert response.status_code == 200


def test_post_notempty_comment(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "9662222222",
                "email": "any@mail.ru",
                "comment": "hi dude"
                }
            )
    assert response.status_code == 200


def test_post_wrong_comment(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "9662222222",
                "email": "iam@spammer.su",
                "comment": "visit www.xxx-girls.com"
                }
            )
    assert response.status_code == 422


def test_post_wrong_email(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "9663333333",
                "email": "some_.ru@mail.cn-strange.namespam@mail.com",
                "comment": "hi dude"
                }
            )
    assert response.status_code == 422


def test_post_terrible_email(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "9664444444",
                "email": "some_.rumail.cn-strange.namespam@mail.com",
                "comment": "what a horror email"
                }
            )
    assert response.status_code == 200


def test_post_wrong_phone(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "666 111 2 ",
                "email": "wrong@phone.kz",
                "comment": "wrong phone number"
                }
            )
    assert response.status_code == 422


def test_post_terrible_phone(test_app):
    response = test_app.post(
            "/user/",
            json={
                "phone": "(|6-66. 555-   55 55",
                "email": "strange@phone.kz",
                "comment": "terrible phone number"
                }
            )
    assert response.status_code == 200


def test_post_send_image(test_app):
    filepath = os.path.abspath('nyancat.jpg')

    with open(filepath, 'rb') as file_:
        filedata = file_.read()

    response = test_app.post(
            "/user/image/",
            files={
                "file": filedata
                }
            )
    assert response.status_code == 200
