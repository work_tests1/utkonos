import logging
from logging.handlers import TimedRotatingFileHandler as TRFH

from app import settings as s


LOG_FILE = s.logfile

Logger = logging.getLogger(__name__)
Logger.setLevel(logging.INFO)


formatter = logging.Formatter(
        '%(asctime)s : %(filename)s : (%(funcName)s)[LINE:%(lineno)d] : %(levelname)s : %(message)s'
        )

file_handler = TRFH(LOG_FILE,
                    when='D',
                    interval=1,
                    backupCount=0)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
Logger.addHandler(file_handler)


console = logging.StreamHandler()
console.setFormatter(formatter)
Logger.addHandler(console)
