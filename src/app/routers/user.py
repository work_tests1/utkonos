# pylint: disable=W1203
'''
    User Module:
        Основной модуль по входящим данным
'''
from fastapi import APIRouter
from fastapi import File, UploadFile
import aiofiles

from app.logger import Logger as logging
from app.database import Session, User
from app.models import (
        UserModel,
        )


router = APIRouter(prefix='/user',
                   tags=['user'])


@router.post('/', summary='user data')
async def reg_user(user: UserModel):
    """ Регистрация пользователя
    """
    logging.info(f"Post received /user User - {user.dict()}")

    user_object = User(**user.dict())
    session = Session()
    session.add(user_object)
    session.commit()

    logging.info(f"Succecfully added {user_object}")
    return {"status": "success"}


@router.post('/image/', summary='Send image')
async def get_image(file: UploadFile = File(...)):
    """ Принимает изображение и сохраняет его в папке /files
        Если таковой нет, то надо создать и дать права на чтение/запись
    """
    logging.info(
            f"Received file: {file.filename}, content-type: {file.content_type}"
            )
    out_file_path = f'/files/{file.filename}'

    async with aiofiles.open(out_file_path, 'wb') as out_file:
        content = await file.read()  # async read
        await out_file.write(content)  # async write

    return out_file_path
