from sqlalchemy import (create_engine,
                        Column,
                        Integer,
                        String,
                        )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError
from app.config import settings as s
from app.logger import Logger as logging


# Создание сессии
ENGINE = create_engine(s.database_uri)
Session = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)

# Подключение базы (с автоматической генерацией моделей)
Base = declarative_base()


class User(Base):
    """ Пользователь
    """
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    phone = Column(String, nullable=True)
    email = Column(String, nullable=True)
    comment = Column(String, nullable=True)

    def __repr__(self):
        return f'<Пользователь  {self.phone}, {self.email}>'

    def __str__(self):
        return f'<Пользователь  {self.phone}, {self.email}>'


try:
    Base.metadata.create_all(bind=ENGINE)

except OperationalError:
    logging.info("Creating test environment")

    ENGINE = create_engine(s.test_database_uri)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)
    Base.metadata.create_all(bind=ENGINE)
