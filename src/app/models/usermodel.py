# pylint: disable=E0213
# pylint: disable=R0201
# pylint: disable=E0611
from typing import Optional
import re
from pydantic import BaseModel, validator
# import phonenumbers
from app.models.urlregexp import MUL_LINE


LEN_PHONE = 10


class UserModel(BaseModel):
    """ Validation Model for data sended
    """

    phone: str
    email: str
    comment: Optional[str] = None

    @validator('phone')
    def check_phone(cls, numbers):
        """ Поле "phone" не должно содержать недопустимых для номера телефона символов,
            а так же должно быть необходимой длины
        """
        cleared = [n for n in repr(numbers) if n.isdigit()]

        if cleared[0] in '78':
            cleared.pop(0)

        if len(cleared) != LEN_PHONE:
            raise ValueError('phone is not correct')

        return '+7' + ''.join(cleared)

    @validator('email')
    def check_mail(cls, mail):
        """ Поле "email" должно соответствовать синтаксису email адреса
            (прим: any@mail.ru)
        """
        reg = r'\b^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b$'
        result = re.search(reg, mail)
        if not result:
            raise ValueError('mail is not correct')

        return result.group(0)

    @validator('comment')
    def check_comment(cls, comment):
        """ Поле "comment".
            Если оно пустое:
                то проходит валидацию,
            иначе:
                необходимо проверить что среди текста не присутствует ссылки,
                например https://www.utkonos.ru/
        """
        result = re.search(MUL_LINE, comment)
        if result:
            raise ValueError('Comment is not correct')

        return comment
