from fastapi import FastAPI

from app.routers import (
        user,
        )

app = FastAPI()


app.include_router(user.router)


@app.get('/')
async def root():
    """ Main Root route
    """
    return {'message': 'I am Root'}
