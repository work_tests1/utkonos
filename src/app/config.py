from pydantic import BaseSettings


class _Settings(BaseSettings):  # pylint: disable=too-few-public-methods

    # DATABASE
    db_user = 'postgres'
    db_pass = 'password'
    db_host = 'postgres'
    db_port = '5432'
    database = 'testcrt'
    database_uri = f'postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:{db_port}/{database}'
    test_database_uri = 'sqlite:///./test.db'

    # LOGFILE
    logfile = '/logs/server.log'


settings = _Settings()
