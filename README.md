[![pipeline status](https://gitlab.com/work_tests1/utkonos/badges/master/pipeline.svg)](https://gitlab.com/work_tests1/utkonos/-/commits/master)
# Backend FastAPI
Необходимо реализовать backend часть, куда будет стучаться клиент

Client - `https://utk-backend-test.web.app/`

[Техническое задание](tz.md)

## Запуск и установка
### Docker
- `docker-compose up`
- Запуск тестов - `docker-compose exec backend pytest .`
### Manual
- Создать и активировать виртуальное окружение `virtualenv venv`
- Установить зависимости `pip install -r requirements.txt`
- Перейти в папку `src`
- Запустить fastapi - `uvicorn app.main:app --host 0.0.0.0 --port 8000`
    > В этом случае содастся локальный файл `test.db` и все записи будут записываться внутри него
- Автотесты - `pytest -vv src/`
    > Либо вручную запустить запросы из консоли \
    > `curl  http://localhost:8000/user/ -X POST -d '{"phone":22222, "email":"my_mail", "comment": "hi dude"}'` \
    > `curl  http://localhost:8000/user/image/ -X POST -F "file=@path/to/file.jpg"`

## Ответ от сервера:
Если ошибка, и статус 422:
```
{
    "detail":[
        {
            "loc":["body","phone"],
            "msg":"phone is not correct",
            "type":"value_error"
        },
        {
            "loc":["body","email"],
            "msg":"mail is not correct",
            "type":"value_error"
        },
        {
            "loc":["body","comment"],
            "msg":"Comment is not correct",
            "type":"value_error"
        }
    ]
}
```
Если успех:
```
    {"status": "success"}
```
