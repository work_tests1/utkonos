# Backend

Необходимо реализовать backend часть, куда будет стучаться клиент

Client - https://utk-backend-test.web.app/
 
## Post `/your_url`
```
body: {
  phone: string,
  email: string,
  comment: string
}
```

### Usage: 
Под формой есть переключатель с тайтлом "Ввести apiUrl" 
После нажатия появится поле для ввода полного адреса, на который будет поступать post запрос

В случае успешной валидации всех полей, ожидается статус 200 (покажется алерт с надписью "Успешно!", форма очистится)

В случае, если одно или несколько полей не прошли валидацию, ожидается 422 статус, а так же тело ответа в виде массива объектов: 

Пример:
```
[
  {
    name: "phone", // имя поля
    error: "Error phone message" // Детализация ошибки (текстовое сообщение)
  },
  {
    name: "email",
    error: "Error email message"
  },
]
```

### Правила валидации: 
Поле "phone" не должно содержать недопустимых для номера телефона символов, а так же должно быть необходимой длины
Поле "email" должно соответствовать синтаксису email адреса (прим: any@mail.ru)
Поле "comment". Если оно пустое, то проходит валидацию, если заполнено, то необходимо проверить что среди текста не присутствует ссылки, например https://www.utkonos.ru/

## Post `/your_url/image`
```
body: binary
```

### Usage: 

В форме есть поле (+) для загрузки изображения, при загрузке бинарные данные отправляются на сервер.
Необходимо вернуть ссылку на изображение, которую можно будет использовать в качестве src
(Ответом приходит просто строка)

В итоговом решение также должен находится README файл, в котором будет описан пайплайн запуска проекта

Плюсом будет использование docker для запуска проекта
Плюсом будет использование postgresql в качестве базы данных в которой будет хранится отвалидированная информация из phone, email, comment
Плюсом будет написать собственный endpoint по получению данных из базы (какие данные получать и как, на собственное усмотрение)
