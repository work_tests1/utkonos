FROM python:3.8-slim as backend
WORKDIR /code
RUN mkdir /files

COPY requirements.txt .
RUN pip install -r requirements.txt && \
    rm -rf /root/.cache/pip

COPY src/ /code
